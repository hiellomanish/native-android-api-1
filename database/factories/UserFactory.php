<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(\App\Model\User::class, function (Faker $faker) {
    $genders = ["male", "female"];
    $gender = $genders[rand(0, 1)];

    $religions = ["Muslim", "Hindu", "Christian", "Jain", "Parsi", "Budhist"];
    $religion = $religions[rand(0, 5)];

    $languages = ["Hindi", "Bengali", "Telugu", "Marathi", "Tamil", "Urdu", "Gujarati"];
    $language = $languages[rand(0, 6)];

    $countries = ["India", "United States", "United Arab", "United Kingdom"];
    $country = $countries[rand(0, 3)];

    return [
        'firstName' => $faker->firstName($gender),
        'lastName' => $faker->lastName($gender),
        'gender' => $gender,
        'mobileNo' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'mainPhotoLink' => $faker->imageUrl(),
        'photoLock' => rand(0, 1),
        'religion' => $religion,
        'motherTongue' => $language,
        'location' => $country,
        'password' => bcrypt('secret'), // secret
        'remember_token' => str_random(10),
    ];
});
