<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OptionsSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$options = $this->getAllOptions();
		foreach ($options as $key => $value) {
			DB::table('options')->insert($value);
		}
	}
	private function getAllOptions()
	{
		$options = [
			'heightField' => $this->heightFieldOptions(),
			'weightField' => $this->weightFieldOptions(),
			'skinColorField' => $this->skinColorFieldOptions(),
			'maritialStatusField' => $this->maritialStatusFieldOptions(),
			'bodyTypeField' => $this->bodyTypeFieldOptions(),
			//'fluentInLanguageField' => $this->fluentInLanguages(),
			'dietField' => $this->dietFieldOptions(),
			'drinkingField' => $this->drinkingSmokingFieldOptions(),
			'smokingField' => $this->drinkingSmokingFieldOptions(),
			'educationLevelField' => $this->educationLevelFieldOptions(),
			// 'graduateSubjectsField' => $this->graduationSubjectFieldOptions(),
			// 'masterGraduateSubjectsField' => $this->masterGraduationSubjectFieldOptions(),
			// 'jobField' => $this->jobFieldOptions(),
			'professionField' => $this->professionFieldOptions(),
			'salaryField' => $this->salaryFieldOptions(),
			'familyValueField' => $this->familyValueFieldOptions(),
			'familyTypeField' => $this->familyTypeFieldOptions(),
			'financialStatusField' => $this->financialStatusFieldOptions(),
			'candidateStatusField' => $this->candidateStatusFieldOptions(),
			'fatherStatusField' => $this->fatherMothersStatusFieldOptions(),
			'motherStatusField' => $this->fatherMothersStatusFieldOptions(),
			'interestField' => $this->interestFieldOptions()
		];
		$optionData = [];
		foreach ($options as $optionFor => $optionsValues) {
			foreach ($optionsValues as $key => $optionsValue) {
				$optionData[] = [
					'optionFor' => $optionFor,
					'options' => $optionsValue
				];
			}
		}
		return $optionData;
	}

// Height Options
	private function heightFieldOptions()
	{
		$options = [
			'4ft - 121.92cm',
			'4ft 1in - 124.46cm',
			'4ft 2in - 127.00cm',
			'4ft 3in - 129.54cm',
			'4ft 4in - 132.08cm',
			'4ft 5in - 134.62cm',
			'4ft 6in - 137.16cm',
			'4ft 7in - 139.70cm',
			'4ft 8in - 142.24cm',
			'4ft 9in - 144.78cm',
			'4ft 10in - 147.32cm',
			'4ft 11in - 149.86cm',
			'5ft - 152.40cm',
			'5ft 1in - 154.94cm',
			'5ft 2in - 157.48cm',
			'5ft 3in - 160.02cm',
			'5ft 4in - 162.56cm',
			'5ft 5in - 165.10cm',
			'5ft 6in - 167.64cm',
			'5ft 7in - 170.18cm',
			'5ft 8in - 172.72cm',
			'5ft 9in - 175.26cm',
			'5ft 10in - 177.80cm',
			'5ft 11in - 180.34cm',
			'6ft - 182.88cm',
			'6ft 1in - 185.42cm',
			'6ft 2in - 187.96cm',
			'6ft 3in - 190.50cm',
			'6ft 4in - 193.04cm',
			'6ft 5in - 195.58cm'
		];
		return $options;

	}


	// Weight Options
	private function weightFieldOptions()
	{
		$options = [
			'30 - 35 kg',
			'35 - 40 kg',
			'40 - 45 kg',
			'45 - 50 kg',
			'50 - 55 kg',
			'55 - 60 kg',
			'60 - 65 kg',
			'65 - 70 kg',
			'70 - 75 kg',
			'75 - 80 kg',
			'80 - 85 kg',
			'85 - 90 kg',
			'90 - 95 kg',
			'95 - 100 kg',
			'100 - 105 kg',
			'105 - 110 kg',
			'110 - 115 kg',
			'115 - 120 kg',
		];
		return $options;

	}
	
	
	// Skin Color options
	private function skinColorFieldOptions()
	{
		$options = ['Very Fair', 'Fair', 'Medium Fair', 'Medium', 'Dark'];
		return $options;
	}
	// Maritial Status
	private function maritialStatusFieldOptions()
	{
		$options = ['Unmarried', 'Divorcee', 'Awaiting Divorce', 'Widow/Widower'];
		return $options;
	}
// Body Type
	private function bodyTypeFieldOptions()
	{
		$options = ['Slim', 'Athletic', 'Average', 'Heavy'];
		return $options;
	}
	

// Diet
	private function dietFieldOptions()
	{
		// TODO: Add All Diet Options
		$options = ['Vegetarian', 'Non Vegetarian', 'Eggetarian'];
		return $options;
	}
	// Drinking and Smoking
	private function drinkingSmokingFieldOptions()
	{
		$options = ['Yes', 'Occasionally', 'No'];
		return $options;
	}
	/********************************************************
	 *  Education Options
	 * *********************************************************/

// Education Level
	private function educationLevelFieldOptions()
	{
		$options = ['Less than 10th/Secondary', '10th/Secondary', 'Plus 2/Higher Secondary', 'Graduation', 'Post Graduation', 'Doctorate'];
		return $options;
	}
	// Graduate Subject




	/********************************************************
	 *  END Education Options
	 * *********************************************************/


	// Profession 
	private function professionFieldOptions()
	{


		$options = ['Not Working', 'Student', 'Pvt. Employee', 'Govt. Employee', 'Self Employed', 'Own Business'];
		return $options;
	}

	

// Salary
	private function salaryFieldOptions()
	{
		$options = ['Upto 1 Lakh per annum', '1 to 2 Lakhs per annum', '2 to 5 Lakhs per annum', '5 to 8 Lakhs per annum', '8 to 10 Lakhs per annum', '10 to 15 Lakhs per annum', '15 to 25 Lakhs per annum', '25 to 50 Lakhs per annum', '50 Lakhs or more per annum'];
		return $options;
	}

	// TODO: Add City State And Country Options
	
	// Family Value
	private function familyValueFieldOptions()
	{
		$options = ['Orthodox/Traditional', 'Moderate', 'Liberal'];
		return $options;
	}

	// FamilyType
	private function familyTypeFieldOptions()
	{
		$options = ['Joint', 'Nuclear'];
		return $options;
	}

	// Financial Status
	private function financialStatusFieldOptions()
	{
		$options = ['Lower middle class (less than 1 crore in assets)', 'Middle class (1 to 5 crore in assets)', 'Upper middle class (5 to 15 crore in assets)', 'Affluent/Wealthy (15 - 50 crore in assets', 'Rich (more than 50 crore in assets)'];
		return $options;
	}
	
	// Candidate Share
	private function candidateStatusFieldOptions()
	{
		$options = ['less than 5 lakhs', '5 to 10 lakhs', '10 to 15 lakhs', '15 to 20 lakhs', '20 to 25 lakhs', '25 to 50 lakhs', '50 lakhs to 1 crore', '1 to 10 crore', '10 crore and above'];
		return $options;
	}
	
	// Father's Mother's Status
	private function fatherMothersStatusFieldOptions()
	{
		$options = ['Employed', 'Business', 'Not working', 'Retired', 'Passed Away'];
		return $options;
	}

    // Father's Mother's Status
    private function interestFieldOptions()
    {
        $options = ['Fiction Reading', 'Non-Fiction Reading', 'Watching Movies', 'Music', 'Writing', 'Watching TV'];
        return $options;
    }



}

