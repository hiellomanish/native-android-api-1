<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = ['comm_m', 'comm_f'];

        foreach($tables as $tbl) {
            Schema::create($tbl, function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('userId1');
                $table->unsignedInteger('userId2');

                $table->boolean('contact_status')->default(false);

                $table->boolean('profileVisits')->default(false);
                $table->dateTime('profileVisitsDate')->nullable();

                $table->boolean('interestMessage')->default(false);
                $table->dateTime('interestMessageDate')->nullable();
                $table->enum('imStatus', ['Unread', 'Pending', 'Accepted', 'Rejected'])->default('Unread');
                $table->dateTime('imStatusDate')->nullable();

                $table->boolean('photoRequest')->default(false);
                $table->dateTime('photoRequestDate')->nullable();
                $table->enum('prStatus', ['Unread', 'Pending', 'Approved', 'Rejected'])->default('Unread');
                $table->dateTime('prStatusDate')->nullable();

                $table->boolean('contactRequest')->default(false);
                $table->dateTime('contactRequestDate')->nullable();
                $table->enum('crStatus', ['Unread', 'Pending', 'Approved', 'Rejected'])->default('Unread');
                $table->dateTime('crStatusDate')->nullable();

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comm_m');
        Schema::dropIfExists('comm_f');
    }
}
