<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = ['user_details_m', 'user_details_f'];

        foreach($tables as $tbl) {
            Schema::create($tbl, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('userId')->unique();
                $table->string('aboutMe');
                $table->string('birthDate');
                $table->string('height');
                $table->string('weight')->nullable();
                $table->string('maritalStatus');
                $table->integer('numOfChildren')->nullable();
                $table->boolean('childLiveWithMe')->nullable();
                $table->string('bodyType');
                $table->string('skinColor')->nullable();
                $table->boolean('physicalDisabilities')->default(false);
                $table->string('caste')->nullable();
                $table->string('subCaste')->nullable();
                $table->string('star')->nullable();
                $table->string('currentCountry');
                $table->string('currentState')->nullable();
                $table->string('currentDistrict')->nullable();
				$table->string('currentTaluk')->nullable();
				$table->string('currentCity')->nullable();
				$table->string('eatingHabits')->nullable();
                $table->string('drinking');
                $table->string('smoking');
                $table->string('hobbies')->nullable();
                $table->text('fluentLanguage')->nullable();
                $table->text('imageLinks')->nullable();

                $table->string('educationLevel');
                $table->string('graduateCourse')->nullable();
				$table->string('graduationSubject')->nullable();
                $table->string('masterCourse')->nullable();
				$table->string('masterSubject')->nullable();
				$table->string('doctorateCourse')->nullable();
				$table->string('doctorateSubject')->nullable();
                $table->string('premierInstitution')->nullable();
                $table->string('profession');
				$table->string('jobFamily')->nullable();
				$table->string('job')->nullable();
                $table->string('salary')->nullable();

                $table->text('aboutMyFamily')->nullable();
                $table->string('familyName')->nullable();
                $table->string('ancestral')->nullable();
                $table->string('familyValue')->nullable();
                $table->string('familyType')->nullable();
                $table->string('financialStatus')->nullable();
                $table->string('candidatesShare')->nullable();
                $table->string('fatherName')->nullable();
                $table->string('fatherStatus')->nullable();
				$table->string('fatherISD')->nullable();
                $table->string('fatherMobileNo')->nullable();
                $table->string('motherName')->nullable();
                $table->string('motherStatus')->nullable();
				$table->string('motherISD')->nullable();
                $table->string('motherMobileNo')->nullable();
				$table->string('landLineISD')->nullable();
                $table->string('parentLandlineNo')->nullable();

                $table->string('familyCity')->nullable();
                $table->string('familyTaluk')->nullable();
				$table->string('familyDistrict')->nullable();
				$table->string('familyState')->nullable();
                $table->string('familyCountry')->nullable();

                $table->integer('noOfBrothers')->nullable();
                $table->integer('noOfBrothersMarried')->nullable();
                $table->integer('noOfSisters')->nullable();
                $table->integer('noOfSistersMarried')->nullable();

                $table->boolean('contactLock')->default(true);
                $table->integer('accountStatus')->default(0);

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details_m');
        Schema::dropIfExists('user_details_f');
    }
}


