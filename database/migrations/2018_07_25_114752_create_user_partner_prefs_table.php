<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPartnerPrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_partner_prefs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->text('aboutMyPartner')->nullable();
            $table->integer('partnerMinAge')->default(0);
            $table->integer('partnerMaxAge')->default(0);
            $table->string('partnerMinHeight')->default(0);
            $table->string('partnerMaxHeight')->default(0);
            $table->text('partnerMaritalStatus')->nullable();
            $table->integer('partnerNoOfChildren')->nullable();
            $table->integer('partnerChildrenLivingWithPartner')->nullable();
			$table->string('partnerEducation')->nullable();
			$table->string('partnerJob')->nullable();
			$table->string('partnerSalary')->nullable();
			$table->string('partnerProfession')->nullable();
			$table->string('partnerGraduateSubject')->nullable();
			$table->string('partnerMastersSubject')->nullable();
			$table->string('partnerFinancialStatus')->nullable();
			$table->string('partnerCandidateShare')->nullable();
            $table->string('partnerBodyType')->nullable();
            $table->string('partnerSkinColor')->nullable();
            $table->string('partnerPhysicalDisabilities')->nullable();
            $table->text('partnerMotherTongue')->nullable();
            $table->string('partnerReligion')->nullable();
			$table->text('partnerCasteAndSubCaste')->nullable();
            $table->string('partnerStar')->nullable();
            $table->text('partnerLocationCountry')->nullable();
            $table->text('partnerLocationState')->nullable();
			$table->text('partnerLocationTaluk')->nullable();
            $table->text('partnerLocationCity')->nullable();
			$table->text('partnerCurrentCountry')->nullable();
			$table->text('partnerCurrentState')->nullable();
			$table->text('partnerCurrentTaluk')->nullable();
			$table->text('partnerCurrentCity')->nullable();
            $table->string('partnerEatingHabits')->nullable();
            $table->string('partnerDrinking')->nullable();
            $table->string('partnerSmoking')->nullable();
            $table->string('partnerFluentIn')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_partner_prefs');
    }
}

