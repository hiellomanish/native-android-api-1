<h5>Hi {{ $name }}</h5>
<p>
    Please verify your email address so we know that it's really you!<br />
    Your verification code is {{ $otp }}
</p>
<p>
    Regards<br />
    Matrimony Team
</p>