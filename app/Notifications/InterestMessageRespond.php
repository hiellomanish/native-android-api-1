<?php

namespace App\Notifications;

use App\Model\Comm;
use App\Model\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;

class InterestMessageRespond extends Notification
{
    use Queueable;

    /** @var  User $user */
    var $user;

    /** @var  Comm $user */
    var $comm;

    /**
     * Create a new notification instance.
     * @param User $user
     * @param Comm $comm
     */
    public function __construct(User $user, Comm $comm)
    {
        $this->user = $user;
        $this->comm = $comm;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FcmChannel::class];
    }

    /**
     * Get the fcm representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toFcm($notifiable)
    {
        // The FcmNotification holds the notification parameters
        /*$fcmNotification = FcmNotification::create()
            ->setTitle('Your account has been activated')
            ->setBody('Thank you for activating your account.');*/


        // The FcmMessage contains other options for the notification
        return FcmMessage::create()
            ->setPriority(FcmMessage::PRIORITY_HIGH)
            ->setTimeToLive(86400)
            ->setData((object) [ 'message' => "{$this->user->firstName} {$this->user->lastName} {$this->comm->imStatus} your interest message" ]);
//            ->setNotification($fcmNotification);
    }
}
