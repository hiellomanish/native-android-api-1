<?php

namespace App\Http\Controllers;

use App\Model\JobOption;
use Illuminate\Http\Request;

class JobOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\JobOption  $jobOption
     * @return \Illuminate\Http\Response
     */
    public function show(JobOption $jobOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\JobOption  $jobOption
     * @return \Illuminate\Http\Response
     */
    public function edit(JobOption $jobOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\JobOption  $jobOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobOption $jobOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\JobOption  $jobOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobOption $jobOption)
    {
        //
    }
}
