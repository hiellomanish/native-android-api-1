<?php

namespace App\Http\Controllers;

use App\Model\UserPartnerPref;
use Illuminate\Http\Request;

class UserPartnerPrefController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\UserPartnerPref  $userPartnerPref
     * @return \Illuminate\Http\Response
     */
    public function show(UserPartnerPref $userPartnerPref)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\UserPartnerPref  $userPartnerPref
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPartnerPref $userPartnerPref)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\UserPartnerPref  $userPartnerPref
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPartnerPref $userPartnerPref)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\UserPartnerPref  $userPartnerPref
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPartnerPref $userPartnerPref)
    {
        //
    }
}
