<?php
/**
 * Created by PhpStorm.
 * User: msp
 * Date: 27/08/18
 * Time: 5:56 PM
 */

namespace App\Http\Controllers\API;


use App\Model\Comm;
use App\Model\FComm;
use App\Model\FUser;
use App\Model\MComm;
use App\Model\MUser;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function overview(Request $request) {
//        $user = $request->user();
//        $isMale = $user->gender == 'male';
//        $id = $user->id;
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $userClass = $isMale ? User::class : User::class;
        $commClass = $isMale ? MComm::class : FComm::class;
        $altCommClass = $isMale ? FComm::class : MComm::class;

        $visitedMeTotal = $altCommClass::where('userId2', $id)->where('profileVisits', '!=', 0)->count();
        $visitedMeNew = $altCommClass::where('userId2', $id)->where('profileVisits', '=', 1)->count();
        $iVisitedTotal = $commClass::where('userId1', $id)->where('profileVisits', '!=', 0)->count();
        $iVisitedToday = $commClass::where('userId1', $id)->where('profileVisits', '!=', 0)->whereDate('profileVisitsDate', Carbon::today())->count();

        $imReceivedTotal = $altCommClass::where('userId2', $id)->where('interestMessage', true)->count();
        $imReceivedNew = $altCommClass::where('userId2', $id)->where('interestMessage', true)->where('imStatus', Comm::STATUS_UNREAD)->count();
        $imSentTotal = $commClass::where('userId1', $id)->where('interestMessage', true)->count();
        $imSentAccepted = $commClass::where('userId1', $id)->where('interestMessage', true)->where('imStatus', Comm::STATUS_ACCEPTED)->count();

        $prReceivedTotal = $altCommClass::where('userId2', $id)->where('photoRequest', true)->count();
        $prReceivedNew = $altCommClass::where('userId2', $id)->where('photoRequest', true)->where('prStatus', Comm::STATUS_UNREAD)->count();
        $prSentTotal = $commClass::where('userId1', $id)->where('photoRequest', true)->count();
        $prSentAccepted = $commClass::where('userId1', $id)->where('photoRequest', true)->where('prStatus', Comm::STATUS_APPROVED)->count();

        $crReceivedTotal = $altCommClass::where('userId2', $id)->where('contactRequest', true)->count();
        $crReceivedNew = $altCommClass::where('userId2', $id)->where('contactRequest', true)->where('crStatus', Comm::STATUS_UNREAD)->count();
        $crSentTotal = $commClass::where('userId1', $id)->where('contactRequest', true)->count();
        $crSentAccepted = $commClass::where('userId1', $id)->where('contactRequest', true)->where('crStatus', Comm::STATUS_APPROVED)->count();

        return response()->json([
            'status' => "OK",
            'data' => [
                'pv_visited_me_total' => $visitedMeTotal,
                'pv_visited_me_new' => $visitedMeNew,
                'pv_i_visited_total' => $iVisitedTotal,
                'pv_i_visited_today' => $iVisitedToday,
                'im_received_total' => $imReceivedTotal,
                'im_received_new' => $imReceivedNew,
                'im_sent_total' => $imSentTotal,
                'im_sent_accepted' => $imSentAccepted,
                'pr_received_total' => $prReceivedTotal,
                'pr_received_new' => $prReceivedNew,
                'pr_sent_total' => $prSentTotal,
                'pr_sent_accepted' => $prSentAccepted,
                'cr_received_total' => $crReceivedTotal,
                'cr_received_new' => $crReceivedNew,
                'cr_sent_total' => $crSentTotal,
                'cr_sent_accepted' => $crSentAccepted,
            ]
        ], 200);
    }
}