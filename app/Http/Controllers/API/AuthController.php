<?php

namespace App\Http\Controllers\API;

use App\Mail\MailOTP;
use App\Model\FcmRegistrationId;
use App\Model\FUserDetail;
use App\Model\MUserDetail;
use App\Model\User;
use App\Model\UserPartnerPref;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Laravel\Passport\PersonalAccessTokenResult;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;

class AuthController extends Controller
{
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return $this->error('Some parameters are missing or invalid', Response::HTTP_BAD_REQUEST, $validator->errors());
        }

        $user = (new User())->findForPassport($request->get('username'));

        if(is_null($user)) {
            return $this->error('We don\'t recognise the username-password combination. Please enter the correct details', Response::HTTP_BAD_REQUEST);
        }

        if (!Hash::check($request->password, $user->password)) {
            return $this->error('We don\'t recognise the username-password combination. Please enter the correct details', Response::HTTP_BAD_REQUEST);
        }

        return $this->_login($user, $request->get('username'), $request->get('password'));
    }

    private function _login(User $user, $username, $password) {
        $http = new Client();

        try {
            $response = $http->post(url('/oauth/token'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('PASSWORD_CLIENT_ID'),
                    'client_secret' => env('PASSWORD_CLIENT_SECRET'),
                    'username' => $username,
                    'password' => $password,
                    'scope' => '*',
                ],
            ]);
        } catch (RequestException $e) {
            return $this->error($e->getMessage(), $e->getResponse()->getStatusCode());
        } catch (\Exception $e) {
            return $this->error('Server error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $userArray = $user->toArray();

        if(isset($userArray['mainPhotoLink']) && !empty($userArray['mainPhotoLink'])) {
            $userArray['mainPhotoLink'] = (Str::startsWith($userArray['mainPhotoLink'], 'http://') || Str::startsWith($userArray['mainPhotoLink'], 'https://')) ? $userArray['mainPhotoLink'] : url('storage/photos/' . $userArray['mainPhotoLink']);
        }

        $user->lastLoginAt = Carbon::now()->toDateTimeString();
        $user->save();

        $accessToken = json_decode((string) $response->getBody(), true);

        return $this->success("You have successfully logged in", [
            'access_token' => $accessToken,
            'user' => $userArray
        ]);
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [

            'firstName' => 'required',
            'lastName' => 'required',
			'ISDCode' => 'required',
            'mobileNo' => 'required|unique:users,mobileNo',
            'email' => 'required|unique:users,email',
            'gender' => 'required|in:Male,Female',
            'location' => 'required',
            'religion' => 'required',
            'motherTongue' => 'required',
            'password' => 'required',
            'password_confirmation' => 'confirmed',

            //Personal
            'aboutMe' => 'required',
//            'firstName' => 'required',
//            'lastName' => 'required',
            'birthDate' => 'required|date',
            'height' => 'required',
            'maritalStatus' => 'required',
//            'numOfChildren' => 'required_if:maritalStatus,'.User::MARITAL_STATUS_DIVORCEE,
            'numOfChildren' => [Rule::requiredIf(function() use ($request) {
                return $request->maritalStatus  != User::MARITAL_STATUS_UNMARRIED;
            })],
            'childLiveWithMe' => [Rule::requiredIf(function() use ($request) {
                return $request->maritalStatus  != User::MARITAL_STATUS_UNMARRIED && $request->numOfChildren > 0;
            })],
            'bodyType' => 'required',
            'skinColor' => 'required',
            'physicalDisabilities' => 'required',
//            'motherTongue' => 'required',
//            'religion' => 'required',
            'caste' => 'required',
            'star' => 'required_if:religion,Hindu',
            'currentCountry' => 'required',
            'currentState' => 'required_if:currentCountry,India',
//            'currentDistrict' => 'required_if:currentCountry,India',
//			'currentTaluk' => 'required_if:currentCountry,India',
//            'currentCity' => 'required_if:currentCountry,India', // TODO: uncomment it
            'eatingHabits' => 'required',
            'drinking' => 'required',
            'smoking' => 'required',

            // Professional and Family
            //  Professional
            'educationLevel' => 'required',
			'graduateCourse' => [Rule::requiredIf(function() use ($request) {
                return array_search(trim($request->educationLevel), [User::EDUCATION_LEVEL_GRADUATE, User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
            'graduationSubject' => [Rule::requiredIf(function() use ($request) {
                return array_search(trim($request->educationLevel), [User::EDUCATION_LEVEL_GRADUATE, User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
			'masterCourse' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
			'masterSubject' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
			'doctorateCourse' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_PHD]) !== false;
            })],
            'doctorateSubject' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_PHD]) !== false;
            })],
            'profession' => 'required',
			'jobFamily' => 'required',
            'salary' => 'required',
            //  Family
//            'aboutMyFamily',
//            'familyName',
            'familyValue' => 'required',
            'familyType' => 'required',
            'financialStatus' => 'required',
            'candidatesShare' => 'required',
            'fatherName' => 'required',
            'fatherStatus' => 'required',
            'motherName' => 'required',
            'motherStatus' => 'required',
            'familyCountry' => 'required',
            'familyState' => 'required_if:familyCountry,India',
//            'familyDistrict' => 'required_if:familyCountry,India', // TODO: uncomment it
//			'familyTalulk' => 'required_if:familyCountry,India',
//            'familyCity' => 'required_if:familyCountry,India', // TODO: uncomment it
            'noOfBrothers' => 'required',
            'noOfBrothersMarried' => [Rule::requiredIf(function() use ($request) {
                return $request->noOfBrothers > 0;
            })],
            'noOfSisters' => 'required',
            'noOfSistersMarried' => [Rule::requiredIf(function() use ($request) {
                return $request->noOfSisters > 0;
            })],

            // Contact Info
//            'mobileNo',
//            'email',
            'parentLandlineNo' => 'required_without_all:fatherMobileNo,motherMobileNo', // missing
            'fatherMobileNo' => 'required_without_all:parentLandlineNo,motherMobileNo', // missing, but found `parentMobileNo`
            'motherMobileNo' => 'required_without_all:fatherMobileNo,parentLandlineNo', // missing, but found `parentMobileNo`
        ]);

        if ($validator->fails()) {
            return $this->error('Some parameters are missing or invalid', Response::HTTP_BAD_REQUEST, $validator->errors());
        }

        DB::beginTransaction();

        $user = new User();
        $user->password = bcrypt($request->password);
        $user->fill($request->only([
            'firstName',
            'lastName',
            'gender',
			'ISDCode',
            'mobileNo',
            'email',
            'religion',
            'motherTongue',
            'location',
            'verified'
        ]));
        $user->save();

        if($request->gender == 'Male') {
            $detail = new MUserDetail();
        } else {
            $detail = new FUserDetail();
        }

        $detail->userId = $user->id;
        $detail->birthDate = date('Y-m-d', strtotime($request->get('birthDate')));
        $detail->fill($request->only([
            'aboutMe',
//            'firstName',
//            'lastName',
//            'birthDate',
            'height',
            'weight',
            'maritalStatus',
            'numOfChildren',
            'childLiveWithMe',
            'bodyType',
            'skinColor',
            'physicalDisabilities',
//            'motherTongue',
//            'religion',
            'caste', // missing
            'subCaste', // missing
            'star', // missing
            'currentCity',
            'currentState',
            'currentDistrict', // missing
			'currentTaluk',
            'currentCountry',
            'eatingHabits',  // missing
            'drinking',
            'smoking',
            'hobbies',
            'fluentLanguage',
            // Professional
            'educationLevel',
            'graduateCourse',
            'graduationSubject',
            'masterCourse',
			'masterSubject',
			'doctorateCourse',
			'doctorateSubject',
            'premierInstitution',
            'profession',
			'jobFamily',
			'job',
            'salary',
            //Family
            'aboutMyFamily',
            'familyName',
            'familyValue',
            'familyType',
            'financialStatus',
            'candidatesShare',  // missing
            'fatherName',
            'fatherStatus',
            'motherName',
            'motherStatus',
            'familyCountry',
            'familyState',
            'familyDistrict', // missing
            'familyTaluk',
			'familyCity',
            'noOfBrothers',
            'noOfBrothersMarried',
            'noOfSisters',
            'noOfSistersMarried',
            //Contact
			'landLineISD',
            'parentLandlineNo',
			'fatherISD',
            'fatherMobileNo',
			'motherISD',
            'motherMobileNo',
        ]));
        $detail->save();

        $partnerPref = new UserPartnerPref();
        $partnerPref->userId = $user->id;
        $partnerPref->fill($request->only([
            'aboutMyPartner',
            'partnerMinAge',
            'partnerMaxAge',
            'partnerMinHeight',
            'partnerMaxHeight',
            'partnerMaritalStatus',
            'partnerNoOfChildren',
            'partnerChildrenLivingWithPartner',
			'partnerEducation',
			'partnerJob',
			'partnerSalary',
			'partnerProfession',
			'partnerGraduateSubject',
			'partnerMastersSubject',
			'partnerFinancialStatus',
			'partnerCandidateShare',
			'partnerBodyType', // missing
            'partnerSkinColor', // missing
            'partnerPhysicalDisabilities', // missing
            'partnerMotherTongue',
            'partnerReligion',
			'partnerCasteAndSubCaste',
            'partnerStar', // missing
            'partnerLocationCountry',
            'partnerLocationState',
//            'partnerLocationDistrict', // missing
            'partnerLocationCity',
            'partnerEatingHabits', // missing, but found `partnerDiet`
            'partnerDrinking',
            'partnerSmoking',
            'partnerFluentIn' // missing
        ]));
        $partnerPref->save();

        DB::commit();

        /** @var PersonalAccessTokenResult $token */
       /* $token = $user->createToken('App Token');

        $accessToken = ['access_token' => $token->accessToken];

        return $this->success("You have successfully registered", [
            'access_token' => $accessToken,
            'user' => $user
        ]);*/

        return $this->_login($user, $request->get('email'), $request->get('password'));
//        return $this->success('You have registered successfully', $user);
    }

    public function user() {
        $user = Auth::user();

        /*if(isset($user->mainPhotoLink) && !empty($user->mainPhotoLink)) {
            $user->mainPhotoLink = (Str::startsWith($user->mainPhotoLink, 'http://') || Str::startsWith($user->mainPhotoLink, 'https://')) ? $user->mainPhotoLink : url('storage/photos/' . $user->mainPhotoLink);
        }*/

        $user->mainPhotoLink = url('storage/photos/' . $user->mainPhotoLink);

        return $this->success("Logged user", $user);
    }

    public function sendMailOTP(Request $request) {
        $validator = Validator::make($request->all(), [
//            'name' => 'required',
            'email' => 'required|email',
            'otp' => 'required'
        ]);

        if($validator->fails()) {
            return $this->error('Some parameters are missing or invalid', Response::HTTP_BAD_REQUEST, $validator->errors());
        }

        try {
            Mail::to($request->email)->sendNow(new MailOTP($request->name, $request->otp));
            return $this->success('Successfully sent');
        } catch(\Exception $e) {
            return $this->error('Error while sending otp', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function verified() {
        $user = Auth::user();
        $user->verified = 1;
        $user->save();

        return $this->success('Successfully verified');
    }

    public function logout(Request $request) {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required',
            'device_id' => 'required'
        ]);

        if($validator->fails()) {
            return $this->error('Some parameters are missing or invalid', Response::HTTP_BAD_REQUEST, $validator->errors());
        }

        $bearerToken = $request->bearerToken();
        $id = (new Parser())->parse($bearerToken)->getHeader('jti');

        /** @var Token $token */
        $token = $request->user()->tokens()->find($id);
        $token->revoke();

        // Delete old registration ids of same device
        FcmRegistrationId::where('device_type', $request->get('device_type'))
            ->where('device_id', $request->get('device_id'))
            ->delete();

        return $this->success('Your device unregistered successfully');
    }
}


