<?php

namespace App\Http\Controllers\API;

use App\Model\Country;
use App\Model\DistrictAndCity;
use App\Model\EducationOption;
use App\Model\JobOption;
use App\Model\StarOption;
use App\Model\Language;
use App\Model\Option;
use App\Model\Religion;
use App\Model\State;

class OptionController extends Controller
{

    public function index() {
        $religions = $this->getReligions();

        $data = [
            'religions' => $religions['religions'],
            'casts' => $religions['casts'],
            'languages' => $this->getLanguages(),
			'job_options' => $this->getJobOptions(),
			'star_options' => $this->getStarOptions(),
            'education_options' => $this->getEducationOptions(),
            'countries' => $this->getCountries(),
            'states' => $this->getStates(),
			'statesAndDistricts' => $this->getStatesAndDistricts(),
            'districtAndCities' => $this->getDistrictAndCities(),
            'options' => $this->getOptions(),
        ];

        /*$data = [];

        $requiredOptions = [
            'religions',
            'languages'
        ];

        foreach($requiredOptions as $requiredOption) {
            $mehodName = 'get'.ucfirst(camel_case($requiredOption));
            $data[$requiredOption] = $this->$mehodName();
        }*/

        return $this->success('', $data);
    }

    private function getReligions() {
        $religions = Religion::all();
        $religions = $religions->groupBy(['religion', 'mainCategory']);
        $religions = $religions->map(function($item) {
            $item = $item->filter(function ($value, $key) {
                return !empty($key);
            });

            return $item->map(function($item, $key) {
                return ['name' => $key, 'options' => $item];
            })->values();
        });

        return ['religions' => $religions->keys(), 'casts' => $religions];
    }

	
    private function getLanguages() {
        return Language::orderBy('order')->get();
    }

	private function getStarOptions() {
        return StarOption::all();
    }
	
	private function getJobOptions() {
        $jobOptions = JobOption::all();
        $jobOptions = $jobOptions->groupBy('jobFamily');
        $jobOptions = $jobOptions->map(function($item, $key) {
            return ['name' => $key, 'options' => $item];
        });
        return $jobOptions->values();
    }

		
    private function getEducationOptions() {
        $educationOptions = EducationOption::all();
        $educationOptions = $educationOptions->groupBy(['optionFor', 'heading']);
        $educationOptions = $educationOptions->map(function($item) {
            return $item->map(function($item, $key) {
                return ['name' => $key, 'options' => $item];
            })->values();
        });
        return $educationOptions;
    }

    private function getCountries() {
        return Country::orderBy('id')->get();
    }

	
    private function getStates() {
        $states = State::all();
        $states = $states->groupBy(['countryId', 'state']);
		$states = $states->map(function($item) {
            return $item->map(function($item, $key) {
                return ['name' => $key, 'options' => $item];
            })->values();
        });
        return $states;
    }
	
	private function getStatesAndDistricts() {
        $statesAndDistricts = State::all();
        $statesAndDistricts = $statesAndDistricts->groupBy(['countryId', 'state']);
		$statesAndDistricts = $statesAndDistricts->map(function($item) {
            return $item->map(function($item, $key) {
                return ['name' => $key, 'options' => $item];
            })->values();
        });
        return $statesAndDistricts;
    }

    private function getDistrictAndCities() {
        $districtAndCities = DistrictAndCity::all();
        $districtAndCities = $districtAndCities->groupBy(['districtId', 'taluk']);
        $districtAndCities = $districtAndCities->map(function($item) {
            return $item->map(function($item, $key) {
                return ['name' => $key, 'options' => $item];
            })->values();
        });
        return $districtAndCities;
    }

    private function getOptions() {
        $options = Option::select('id', 'optionFor', 'options as option')->get();
        $options = $options->groupBy('optionFor');
        return $options;
    }
	
	
}



