<?php

namespace App\Http\Controllers\API;

use App\Model\FcmRegistrationId;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FcmController extends Controller
{

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required',
            'device_id' => 'required',
            'registration_id' => 'required'
        ]);

        if($validator->fails()) {
            return $this->error('Some parameters are missing or invalid', Response::HTTP_BAD_REQUEST, $validator->errors());
        }

        // Delete old registration ids of same device
        FcmRegistrationId::where('device_type', $request->get('device_type'))
            ->where('device_id', $request->get('device_id'))
            ->delete();

        $fcm = new FcmRegistrationId();
        $fcm->user_id = Auth::id();
        $fcm->fill($request->only([
            'device_type',
            'device_id',
            'registration_id'
        ]));
        $fcm->save();

        return $this->success('Your device registered successfully');
    }

    public function unRegister(Request $request) {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required',
            'device_id' => 'required'
        ]);

        if($validator->fails()) {
            return $this->error('Some parameters are missing or invalid', Response::HTTP_BAD_REQUEST, $validator->errors());
        }

        // Delete old registration ids of same device
        FcmRegistrationId::where('device_type', $request->get('device_type'))
            ->where('device_id', $request->get('device_id'))
            ->delete();

        return $this->success('Your device unregistered successfully');
    }
}
