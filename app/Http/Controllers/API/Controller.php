<?php
/**
 * Created by PhpStorm.
 * User: msp
 * Date: 29/08/18
 * Time: 10:17 AM
 */

namespace App\Http\Controllers\API;


use Illuminate\Http\Response;

class Controller
{
    function success($message, $data = null, $statusCode = Response::HTTP_OK) {
        return response()->json([
            'message' => $message,
            'data' => $data,
        ], $statusCode);
    }

    function error($message = null, $statusCode, $error = null) {
        return response()->json([
            'message' => $message,
            'error' => $error
        ], $statusCode);
    }
}