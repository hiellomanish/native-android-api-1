<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPartnerPref extends Model
{
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'partnerMaritalStatus' => 'array',
        'partnerMotherTongue' => 'array',
        'partnerReligion' => 'array',
        'partnerStar' => 'array',
        'partnerSmoking' => 'array',
        'partnerDrinking' => 'array',
        'partnerBodyType' => 'array',
        'partnerSkinColor' => 'array',
        'partnerEatingHabits' => 'array',
		'partnerEducation' => 'array',
		'partnerJob' => 'array',
		'partnerSalary' => 'array',
		'partnerProfession' => 'array',
		'partnerGraduateSubject' => 'array',
		'partnerMastersSubject' => 'array',
		'partnerFinancialStatus' => 'array',
		'partnerCandidateShare' => 'array',
        'partnerFluentIn' => 'array',
    ];
}

