<?php

namespace App\Model;

use App\User;

class MUser extends User
{
    protected $table = 'users_m';


    public function comm()
    {
        return $this->hasOne(MComm::class, 'userId1', 'id');
    }
}
