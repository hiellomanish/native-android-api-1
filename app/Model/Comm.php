<?php
/**
 * Created by PhpStorm.
 * User: msp
 * Date: 27/08/18
 * Time: 6:12 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

abstract class Comm extends Model
{
    const TYPE_PV_I_VISITED = 1;
    const TYPE_PV_VISITED_ME = 2;
    const TYPE_IM_SENT = 3;
    const TYPE_IM_RECEIVED = 4;
    const TYPE_PR_SENT = 5;
    const TYPE_PR_RECEIVED = 6;
    const TYPE_CR_SENT = 7;
    const TYPE_CR_RECEIVED = 8;

    const STATUS_UNREAD = 'Unread';
    const STATUS_PENDING = 'Pending';
    const STATUS_ACCEPTED = 'Accepted';
    const STATUS_APPROVED = 'Approved';
    const STATUS_REJECTED = 'Rejected';

    const CONTACT_STATUS_NONE = 0;
    const CONTACT_STATUS_IGNORED = 1;
    const CONTACT_STATUS_SHORTLISTED = 2;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'userId1');
    }

    public function tableName() {
        return $this->table;
    }
}