<?php

namespace App\Model;

use App\User;

class FUser extends User
{
    protected $table = 'users_f';

    public function comm()
    {
        return $this->hasOne(FComm::class, 'userId1', 'id');
    }
}
