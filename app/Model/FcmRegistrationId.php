<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FcmRegistrationId extends Model
{
    protected $guarded = ['id'];
}
