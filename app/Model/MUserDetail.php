<?php

namespace App\Model;

class MUserDetail extends UserDetail
{
    const TABLE_NAME = 'user_details_m';
    protected $table = MUserDetail::TABLE_NAME;
    protected $guarded = [];
}
