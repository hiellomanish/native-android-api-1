<?php
/**
 * Created by PhpStorm.
 * User: msp
 * Date: 27/08/18
 * Time: 6:13 PM
 */

namespace App\Model;


class MComm extends Comm
{
    const TABLE_NAME = 'comm_m';
    protected $table = MComm::TABLE_NAME;

    public function user1()
    {
        return $this->belongsTo(User::class, 'userId1');
    }

    public function user2()
    {
        return $this->belongsTo(User::class, 'userId2');
    }
}